#!/bin/bash
op1=$1

dia=`date +%d`
mes=`date +%m`
ano=`date +%y`

if [[ $#>1 ]]; then
	echo "SOLO SE ADMITE UN PARAMETRO"
	exit 1
fi

if [[ $1 == "-s" || $1 == "--short" ]]; then
	echo "$dia/$mes/$ano"
	
elif [[ $1 == "-l" || $1 == "--long" ]]; then
	echo "Hoy es el dia $dia del mes $mes del año $ano"
	
elif [[ $1 == "" ]]; then
	cal
	
fi

if [[ $op1 != "-s" && $op1 != "--short" && $op1 != "--long" && $op1 != "-l" && $op1 != "" ]]; then
	echo "opcion incorrecta, solo se acepta el parametro -s -l --short --long"
	exit 1
fi

