#!/bin/bash

nombre=$1
if [ ! -d $nombre ]; then
	mkdir $nombre

else
	echo"el directorio ya esta creado, se hara un cambio de nombre"
	read name
	mv $nombre $name
	nombre=$name
fi	

cd $nombre

touch 1t4v_docking.mol2
touch 1t4v_referencia.mol2
touch 2r2m_docking.mol2
touch 2r2m_referencia.mol2
touch 3ldx_docking.mol2
touch 3ldx_docking_new.mol2
touch 3ldx_referencia.mol2
touch 3ldx_referencia_new.mol2
